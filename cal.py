#!/bin/python3
import statistics 
import numpy as np
from decimal import Decimal


with open('testfile', 'r') as f:
    queue_list = f.readlines()

queue_list = [ float(i) for i in queue_list]
queue_list.pop(0)
# queue_list = queue_list[100:]


print("Mean : "+str(float("%0.9f" %statistics.mean(queue_list)))+" ms")
print("Median : "+str(float("%0.9f" %statistics.median(queue_list)))+" ms")
print("Standard Deviation : "+str(float("%0.9f" %statistics.stdev(queue_list)))+" ms")
print("Max : "+ str(float("%0.9f" %max(queue_list))) +" ms")
print("Min : "+ str(float("%0.9f" %min(queue_list))) +" ms")
print("90th percentile : "+str(float("%0.9f" %np.percentile(queue_list,90)))+" ms")
print("99th percentile : "+str(float("%0.9f" %np.percentile(queue_list,99)))+" ms")
print("99.9th percentile : "+str(float("%0.9f" %np.percentile(queue_list,99.9)))+" ms")
print("99.99th percentile : "+str(float("%0.9f" %np.percentile(queue_list,99.99)))+" ms")
print("99.999th percentile : "+str(float("%0.9f" %np.percentile(queue_list,99.999)))+" ms")
print("Packet count: "+ str(len(queue_list)))
# print("Total Packet count: "+ str(Totalcount))


queue_list.sort()
del queue_list[int(len(queue_list)-((len(queue_list)/100)*2)):len(queue_list)]
# #print(len(queue_list))
#queue_list = queue_list[:len(queue_list)-100]
print("-------------Without Top 100 outliers ----------------")

print("*Mean : "+str(float("%0.9f" %statistics.mean(queue_list)))+" ms")
print("*Median : "+str(float("%0.9f" %statistics.median(queue_list)))+" ms")
print("*Standard Deviation : "+str(float("%0.9f" %statistics.stdev(queue_list)))+" ms")
print("*Max : "+ str(float("%0.9f" %max(queue_list))) +" ms")
print("*Min : "+ str(float("%0.9f" %min(queue_list))) +" ms")
print("*90th percentile : "+str(float("%0.9f" %np.percentile(queue_list,90)))+" ms")
print("*99th percentile : "+str(float("%0.9f" %np.percentile(queue_list,99)))+" ms")
print("*Packet count: "+ str(len(queue_list)))

queue_list.sort()
del queue_list[int(len(queue_list)-((len(queue_list)/100)*1)):len(queue_list)]
# #print(len(queue_list))
#queue_list = queue_list[:len(queue_list)-100]
# print("-------------Without Top 100 outliers ----------------")

# print("Mean : "+str(float("%0.9f" %statistics.mean(queue_list)))+" ms")
# print("Median : "+str(float("%0.9f" %statistics.median(queue_list)))+" ms")
# print("Standard Deviation : "+str(float("%0.9f" %statistics.stdev(queue_list)))+" ms")
# print("Max : "+ str(float("%0.9f" %max(queue_list))) +" ms")
# print("Min : "+ str(float("%0.9f" %min(queue_list))) +" ms")
# print("90th percentile : "+str(float("%0.9f" %np.percentile(queue_list,90)))+" ms")
# print("99th percentile : "+str(float("%0.9f" %np.percentile(queue_list,99)))+" ms")
# print("Packet count: "+ str(len(queue_list)))



