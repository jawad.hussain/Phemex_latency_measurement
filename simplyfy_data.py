import sys
import re


 
# Arguments passed
# if len(sys.argv) >1:
#     print("\nFile Path:", sys.argv[1])

iterations = float(sys.argv[2])

cpp_ker_median = 0
cpp_xstack_median = 0
java_ker_median = 0
java_xstack_median = 0

cpp_ker_stddev = 0
cpp_xstack_stddev = 0
java_ker_stddev = 0
java_xstack_stddev = 0

with open(sys.argv[1], 'r') as f:
    queue_list = f.readlines()
mode = "null"
for line in queue_list:
    if "cpp kernel" in line:
        mode = "cpp kernel"
        continue
    elif "cpp xstack" in line:
        mode = "cpp xstack"
        continue
    elif "Java application on kernel" in line:
        mode = "Java application on kernel"
        continue
    elif "Java application on xstack" in line:
        mode = "Java application on xstack"
        continue
    elif mode == "cpp kernel":
        if "*Median :" in line:
            x = line.split(":")[1]
            cpp_ker_median = cpp_ker_median + float(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", x)[0])
        elif "*Standard Deviation :" in line:
            x = line.split(":")[1]
            cpp_ker_stddev = cpp_ker_stddev + float(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", x)[0])
    
    elif mode == "cpp xstack":
        if "*Median :" in line:
            x = line.split(":")[1]
            cpp_xstack_median = cpp_xstack_median + float(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", x)[0])
        elif "*Standard Deviation :" in line:
            x = line.split(":")[1]
            cpp_xstack_stddev = cpp_xstack_stddev + float(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", x)[0])
    
    elif mode == "Java application on kernel":
        if "*Median :" in line:
            x = line.split(":")[1]
            java_ker_median = java_ker_median + float(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", x)[0])
        elif "*Standard Deviation :" in line:
            x = line.split(":")[1]
            java_ker_stddev = java_ker_stddev + float(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", x)[0])
    
    elif mode == "Java application on xstack":
        if "*Median :" in line:
            x = line.split(":")[1]
            java_xstack_median = java_xstack_median + float(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", x)[0])
        elif "*Standard Deviation :" in line:
            x = line.split(":")[1]
            java_xstack_stddev = java_xstack_stddev + float(re.findall(r"[-+]?(?:\d*\.\d+|\d+)", x)[0])  

# print (cpp_ker_median,cpp_xstack_median,java_ker_median,java_xstack_median ,cpp_ker_stddev ,cpp_xstack_stddev ,java_ker_stddev ,java_xstack_stddev)


cpp_ker_median = cpp_ker_median/iterations
cpp_xstack_median = cpp_xstack_median/iterations
java_ker_median = java_ker_median/iterations
java_xstack_median = java_xstack_median/iterations
cpp_ker_stddev = cpp_ker_stddev/iterations
cpp_xstack_stddev = cpp_xstack_stddev/iterations
java_ker_stddev = java_ker_stddev/iterations
java_xstack_stddev = java_xstack_stddev/iterations

# print (cpp_ker_median,cpp_xstack_median,java_ker_median,java_xstack_median ,cpp_ker_stddev ,cpp_xstack_stddev ,java_ker_stddev ,java_xstack_stddev)

print ("\n********** Average Numbers **********\n")
print("cpp Kernal:")
print("Median : " + str(cpp_ker_median) + " ms")
print("Standard Deviation : " + str(cpp_ker_stddev) + " ms\n")
print("cpp xstack:")
print("Median : " + str(cpp_xstack_median) + " ms")
print("Standard Deviation : " + str(cpp_xstack_stddev) + " ms\n")
print("Java Kernal:")
print("Median : " + str(java_ker_median)+ " ms")
print("Standard Deviation : " + str(java_ker_stddev) + " ms\n")
print("Java xstack:")
print("Median : " + str(java_xstack_median) + " ms")
print("Standard Deviation : " + str(java_xstack_stddev) + " ms\n")

print("********** Improvement **********\n")
print("cpp Kernal vs cpp Xstack:")
print("Median : " + str(cpp_ker_median - cpp_xstack_median) + " ms")
print("Standard Deviation : " + str(cpp_ker_stddev - cpp_xstack_stddev) + " ms\n")

print("Java Kernal vs Java Xstack:")
print("Median : " + str(java_ker_median - java_xstack_median) + " ms")
print("Standard Deviation : " + str(java_ker_stddev - java_xstack_stddev) + " ms\n")
