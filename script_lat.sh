#!/bin/bash


#set parameters
DATE=`date +"%F_%T"`
TIME=`date +"%T"`
ITERATIONS=3
VALUES_PER_ITER=1000
LOG_FOLDER=latency_logs
LOG_FILE=latency_$DATE
INTERFACE=ens6

mkdir $LOG_FOLDER
cd $LOG_FOLDER
touch $LOG_FILE
echo $LOG_FOLDER/$LOG_FILE
TEMP_FILE="testfile"

# echo "iteration, time, simple_kernel, simple_xstack_master, simple_xstack_dev, java_kernel, jave_xstack_master, jave_xstack_dev" > $LOG_FILE


SIMPLE="../latency_ping https://api.phemex.com /md/orderbook?symbol=BTCUSD $VALUES_PER_ITER"
JAVA="java -jar ../latencytestsuite-0.1-latencytestsuite-distribution.jar -url \"https://api.phemex.com//md/orderbook?symbol=BTCUSD\" -timeoutms 0 -count $VALUES_PER_ITER"

SIMPLE_GREP="Response received with latency: "
JAVA_GREP=" - received response in"
DEV=/home/ubuntu/stack_bypass/scripts
LIB="/home/ubuntu/stack_bypass/build/src/libstackbypass_6.a"
XSTACK_DEV="LD_PRELOAD=/home/ubuntu/stack_bypass/build/src/libstackbypass_6.a"
XSTACK_MASTER="sudo xstack -i=$INTERFACE "

echo "Warming up path"
for (( i=1; i<= 2; i++ ))
do
      $SIMPLE | grep "$SIMPLE_GREP" | awk '{print $5}' |awk '{print ($0+0)}'| sort -n > $TEMP_FILE 
      $XSTACK_MASTER $SIMPLE | grep "$SIMPLE_GREP" | awk '{print $5}' |awk '{print ($0+0)}'| sort -n > $TEMP_FILE
      $JAVA | grep "$JAVA_GREP" | awk '{print $9}' | sed 's/MS//g' | sort -n > $TEMP_FILE
      $XSTACK_MASTER $JAVA | grep "$JAVA_GREP" | awk '{print $9}' | sed 's/MS//g' | sort -n > $TEMP_FILE
done 

echo "Start"

for (( i=1; i<= $ITERATIONS; i++ ))
do
      TIME=`date +"%T"`
      $SIMPLE | grep "$SIMPLE_GREP" | awk '{print $5}' |awk '{print ($0+0)}'| sort -n > $TEMP_FILE
      echo "cpp kernel $SIMPLE"
      echo "cpp kernel $SIMPLE" >> $LOG_FILE
      python3 ../cal.py >> $LOG_FILE
      $XSTACK_MASTER $SIMPLE | grep "$SIMPLE_GREP" | awk '{print $5}' |awk '{print ($0+0)}'| sort -n > $TEMP_FILE
      echo "cpp xstack $XSTACK_MASTER $SIMPLE"
      echo "cpp xstack $XSTACK_MASTER $SIMPLE">> $LOG_FILE
      python3 ../cal.py >> $LOG_FILE
      #  echo $S_KERNEL
      #  S_XSTACK_M=$($XSTACK_MASTER $SIMPLE | grep AVERAGE | awk '{print $4}')
      #  echo $S_XSTACK_M
      #  S_XSTACK_D=$($XSTACK_DEV $SIMPLE | grep AVERAGE | awk '{print $4}')
      #  echo $S_XSTACK_D

      $JAVA | grep "$JAVA_GREP" | awk '{print $9}' | sed 's/MS//g' | sort -n > $TEMP_FILE
      echo "Java application on kernel $JAVA"
      echo "Java application on kernel $JAVA" >> $LOG_FILE
      python3 ../cal.py >> $LOG_FILE

      $XSTACK_MASTER $JAVA | grep "$JAVA_GREP" | awk '{print $9}' | sed 's/MS//g' | sort -n > $TEMP_FILE
      echo "Java application on $XSTACK_MASTER $JAVA" 
      echo "Java application on xstack $JAVA">> $LOG_FILE
      python3 ../cal.py >> $LOG_FILE
#
#      echo "$XSTACK_DEV $JAVA"
#      LD_PRELOAD=$LIB $JAVA | grep "$JAVA_GREP" | awk '{print $9}' | sed 's/MS//g' | sort -n > $TEMP_FILE
#      echo "Java application on $XSTACK_DEV $JAVA"
#     echo "Java application on kernel $JAVA">> $LOG_FILE
#      python3 ../cal.py
      echo "********** DONE **********" >> $LOG_FILE
done
      python3 ../simplyfy_data.py $LOG_FILE $ITERATIONS >>  $LOG_FILE



